CREATE TABLE `rating`
(
    `id`       bigint(20) NOT NULL AUTO_INCREMENT,
    `source`   varchar(255) DEFAULT NULL,
    `value`    varchar(255) DEFAULT NULL,
    `movie_id` bigint(20) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `movie_pk` (`movie_id`),
    CONSTRAINT `movie_fk` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1