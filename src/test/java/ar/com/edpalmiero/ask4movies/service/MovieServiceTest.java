package ar.com.edpalmiero.ask4movies.service;

import ar.com.edpalmiero.ask4movies.Ask4MoviesApplicationTests;
import ar.com.edpalmiero.ask4movies.domain.MovieDTO;
import ar.com.edpalmiero.ask4movies.repository.MovieRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

public class MovieServiceTest extends Ask4MoviesApplicationTests {

    private static final String TITLE_NOT_IN_DB_OR_OMDB = "SUPER FAKE MOVIE A";
    private static final String IMDB_ID_NOT_IN_DB = "tt1856010";
    private static final String IMDB_ID_IN_DB = "tt3896198";
    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieRepository movieRepository;

    @Value("${ask4movies.movieService.resultsPerPage}")
    private int fixedLengthResultsPerPage;
    private static final int FIRST_PAGE = 0;
    private static final int FIND_ALL_LENGTH = 2;
    private String TITLE_IN_DB = "Guardians of the Galaxy Vol. 2";
    private String TITLE_NOT_IN_DB = "House";

    @Test
    public void findMovieById_emptyId_illegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> movieService.findById(""));
    }

    @Test
    public void findMovieById_NotInDB_bringResultFromOMDB() {
        assertNotNull(movieService.findById(IMDB_ID_NOT_IN_DB));
    }

    @Test
    public void findMovieById_inDB_bringResult() {
        assertNotNull(movieService.findById(IMDB_ID_IN_DB));
    }

    @Test
    public void findAllByPage_firstPage_returnsFirstPageWithFixedLenghtResults() {
        Page<MovieDTO> page = movieService.findAll(FIRST_PAGE);
        assertTrue(page.getTotalElements() == FIND_ALL_LENGTH, "findAll -> Se trajo la cantidad de resultados necesaria: " + FIND_ALL_LENGTH);
    }


    @Test
    public void findByTitle_foundInDatabaseCache_returnsMovieWithTitle() {
        HashMap<String, String> params = new HashMap<>();
        params.put("title", TITLE_IN_DB);

        Page<MovieDTO> movies = movieService.find(params, FIRST_PAGE);
        assertTrue(movies.getContent().stream().allMatch(movieDTO -> movieDTO.getTitle().equals(TITLE_IN_DB)));
    }

    @Test
    public void findByTitle_notFoundInDatabase_searchInOMDB() {
        HashMap<String, String> params = new HashMap<>();
        params.put("title", TITLE_NOT_IN_DB);

        Pageable pageableRepository = PageRequest.of(FIRST_PAGE, fixedLengthResultsPerPage);
        assertTrue(movieRepository.findFirstByTitleStartingWith(TITLE_NOT_IN_DB, pageableRepository).getContent().isEmpty());

        Page<MovieDTO> movies = movieService.find(params, FIRST_PAGE);
        assertFalse(movies.getContent().isEmpty());
        assertTrue(movies.getContent().stream().allMatch(movieDTO -> movieDTO.getTitle().contains(TITLE_NOT_IN_DB)));
        assertTrue(movieRepository.findFirstByTitleStartingWith(TITLE_NOT_IN_DB, pageableRepository).stream().allMatch(movieDTO -> movieDTO.getTitle().contains(TITLE_NOT_IN_DB)));
        assertEquals(movieRepository.findAll().size(), FIND_ALL_LENGTH + 1);
    }

    @Test
    public void findByTitle_notFoundInDBOrOMDB_returnsEmpty() {
        HashMap<String, String> params = new HashMap<>();
        params.put("title", TITLE_NOT_IN_DB_OR_OMDB);
        assertTrue(movieService.find(params, FIRST_PAGE).getContent().isEmpty());
        assertEquals(movieRepository.findAll().size(), FIND_ALL_LENGTH);
    }

}
