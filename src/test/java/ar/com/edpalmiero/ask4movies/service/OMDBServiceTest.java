package ar.com.edpalmiero.ask4movies.service;

import ar.com.edpalmiero.ask4movies.Ask4MoviesApplicationTests;
import ar.com.edpalmiero.ask4movies.domain.MovieDTO;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

public class OMDBServiceTest extends Ask4MoviesApplicationTests {

    private static final String HOUSE_TITLE = "House";
    private static final String YEAR_OF_RELEASE = "2005";

    @Autowired
    private OMDBService omdbService;


    @SneakyThrows
    @Test
    public void findByTitle_titleExists_returnsMovie() {
        HashMap<String, String> params = new HashMap<>();
        params.put("title", HOUSE_TITLE);
        MovieDTO movieDTO = omdbService.find(params);
        assertNotNull(movieDTO);
        assertTrue(movieDTO.getTitle().equals(HOUSE_TITLE));
    }

    @Test
    public void findByTitleAndYear_emptyTitle_doesntFindByTitle() {
        HashMap<String, String> params = new HashMap<>();
        params.put("title", "");
        MovieDTO movieDTO = omdbService.find(params);
        assertFalse(movieDTO.isResponse());

    }

    @Test
    public void findByTitleAndYear_titleExists_returnsMovie() {
        HashMap<String, String> params = new HashMap<>();
        params.put("title", HOUSE_TITLE);
        params.put("yearOfRelease", YEAR_OF_RELEASE);

        MovieDTO movieDTO = omdbService.find(params);
        assertNotNull(movieDTO);
        assertTrue(movieDTO.getTitle().contains(HOUSE_TITLE));
        assertTrue(LocalDate.ofInstant(movieDTO.getReleased().toInstant(), ZoneId.systemDefault()).getYear() == Integer.valueOf(YEAR_OF_RELEASE));
    }

}
