INSERT INTO movie(Title, Year, Rated,
                  Released, Runtime, Genre, Director, Writer, Actors, Plot, Language, Country,
                  Awards, Poster, Metascore, imdb_Rating, imdb_Votes, imdb_ID, Type,
                  DVD, Box_office, Production, Website)
VALUES ('Guardians of the Galaxy Vol. 2', 2017, 'PG-13',
        '2017-05-05', '136 min', 'Action, Adventure, Comedy, Sci-Fi',
        'James Gunn',
        'James Gunn, Dan Abnett (based on the Marvel comics by), Andy Lanning (based on the Marvel comics by), Steve Englehart (Star-Lord created by), Steve Gan (Star-Lord created by), Jim Starlin (Gamora and Drax created by), Stan Lee (Groot created by), Larry Lieber (Groot created by), Jack Kirby (Groot created by), Bill Mantlo (Rocket Raccoon created by), Keith Giffen (Rocket Raccoon created by), Steve Gerber (Howard the Duck created by), Val Mayerik (Howard the Duck created by)',
        'Chris Pratt, Zoe Saldana, Dave Bautista, Vin Diesel',
        'The Guardians struggle to keep together as a team while dealing with their personal family issues, notably Star-Lord''s encounter with his father the ambitious celestial being Ego.',
        'English',
        'USA', 'Nominated for 1 Oscar. Another 14 wins & 52 nominations.',
        'https://m.media-amazon.com/images/M/MV5BNjM0NTc0NzItM2FlYS00YzEwLWE0YmUtNTA2ZWIzODc2OTgxXkEyXkFqcGdeQXVyNTgwNzIyNzg@._V1_SX300.jpg',
        67, 7.6, '518,435', 'tt3896198', 'movie',
        '2017-08-22', '$389,804,217', 'Walt Disney Pictures', 'N/A');


INSERT INTO rating(source, value, movie_id)
VALUES ('Internet Movie Database', '7.6/10', 1);


INSERT INTO movie(Title, Year, Rated,
                  Released, Runtime, Genre, Director, Writer, Actors, Plot, Language, Country,
                  Awards, Poster, Metascore, imdb_Rating, imdb_Votes, imdb_ID, Type,
                  DVD, Box_office, Production, Website)
VALUES ('Guardians of the Galaxy Vol. 3', 2017, 'PG-13',
        '2019-05-05', '136 min', 'Action, Adventure, Comedy, Sci-Fi',
        'James Gunn',
        'James Gunn, Dan Abnett (based on the Marvel comics by), Andy Lanning (based on the Marvel comics by), Steve Englehart (Star-Lord created by), Steve Gan (Star-Lord created by), Jim Starlin (Gamora and Drax created by), Stan Lee (Groot created by), Larry Lieber (Groot created by), Jack Kirby (Groot created by), Bill Mantlo (Rocket Raccoon created by), Keith Giffen (Rocket Raccoon created by), Steve Gerber (Howard the Duck created by), Val Mayerik (Howard the Duck created by)',
        'Chris Pratt, Zoe Saldana, Dave Bautista, Vin Diesel',
        'The Guardians struggle to keep together as a team while dealing with their personal family issues, notably Star-Lord''s encounter with his father the ambitious celestial being Ego.',
        'English',
        'USA', 'Nominated for 1 Oscar. Another 14 wins & 52 nominations.',
        'https://m.media-amazon.com/images/M/MV5BNjM0NTc0NzItM2FlYS00YzEwLWE0YmUtNTA2ZWIzODc2OTgxXkEyXkFqcGdeQXVyNTgwNzIyNzg@._V1_SX300.jpg',
        67, 7.6, '518,435', 'tt3896178', 'movie',
        '2017-08-22', '$389,804,217', 'Walt Disney Pictures', 'N/A');

INSERT INTO rating(source, value, movie_id)
VALUES ('Internet Movie Database', '7.6/10', 2);