

CREATE TABLE movie
(
    id         bigint primary key auto_increment,
    imdb_id     varchar(255),
    title      varchar(255),
    year       varchar(255),
    rated      varchar(255),
    released   date,
    runtime    varchar(255),
    genre      varchar(255),
    director   varchar(255),
    writer     text,
    actors     varchar(255),
    plot       varchar(255),
    language   varchar(255),
    country    varchar(255),
    awards     varchar(255),
    poster     varchar(255),
    metascore  varchar(255),
    imdb_rating numeric(3, 1),
    imdb_votes  varchar(255),
    type       varchar(255),
    dvd        varchar (255),
    box_office  varchar(255),
    production varchar(255),
    website    VARCHAR(255)
);

CREATE TABLE rating
(
    id       bigint primary key auto_increment,
    movie_id bigint       not null,
    source   VARCHAR(255) NOT NULL,
    value    VARCHAR(255) NOT NULL

);
ALTER TABLE rating ADD CONSTRAINT `movie_FK` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`) ON DELETE CASCADE