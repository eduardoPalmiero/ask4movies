package ar.com.edpalmiero.ask4movies.domain;

public enum TitleType {
    series, movie, episode
}

