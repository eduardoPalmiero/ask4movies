package ar.com.edpalmiero.ask4movies.controller;

import ar.com.edpalmiero.ask4movies.domain.MovieDTO;
import ar.com.edpalmiero.ask4movies.domain.TitleType;
import ar.com.edpalmiero.ask4movies.service.MovieService;
import ar.com.edpalmiero.ask4movies.vo.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

import static org.apache.logging.log4j.util.Strings.isNotBlank;
import static org.apache.logging.log4j.util.Strings.trimToNull;

@RestController
@RequestMapping(value = "/movie")
public class MovieController {
    @Autowired
    private MovieService movieService;

    @GetMapping()
    public PageVO<MovieDTO> findAll(@RequestParam("p") int pageNumber,
                                    @RequestParam(value = "t", required = false) String title,
                                    @RequestParam(value = "y", required = false) String yearOfRelease,
                                    @RequestParam(required = false) TitleType type) {
        HashMap<String, String> params = new HashMap<>();
        params.put("title", trimToNull(title));
        params.put("yearOfRelease", trimToNull(yearOfRelease));
        if (type != null) {
            params.put("type", type.toString().trim());
        }
        if (anyParam(params)) {
            return PageVO.of(movieService.find(params, pageNumber));
        }
        return PageVO.of(movieService.findAll(pageNumber));
    }

    private boolean anyParam(HashMap<String, String> params) {
        return params.entrySet().stream().anyMatch(param -> isNotBlank(param.getValue()));
    }

    @GetMapping("/{imdbID}")
    public MovieDTO findById(@PathVariable String imdbID) {
        return movieService.findById(trimToNull(imdbID));
    }

}
