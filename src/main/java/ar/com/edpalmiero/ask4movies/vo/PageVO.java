package ar.com.edpalmiero.ask4movies.vo;

import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
public class PageVO<T> {

    private int page;
    private int size;
    private List<T> results;

    public static PageVO of(Page page) {
        PageVO pageVo = new PageVO();
        pageVo.setResults(page.getContent());
        pageVo.setPage(page.getNumber());
        pageVo.setSize(page.getSize());
        return pageVo;
    }
}
