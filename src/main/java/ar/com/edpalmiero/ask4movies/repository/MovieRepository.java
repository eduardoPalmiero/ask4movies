package ar.com.edpalmiero.ask4movies.repository;

import ar.com.edpalmiero.ask4movies.domain.MovieDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface MovieRepository extends JpaRepository<MovieDTO, String>, JpaSpecificationExecutor {
    /*This is done to imitate OMDB api behaviour when filtering by title*/
    Page<MovieDTO> findFirstByTitleStartingWith(String title, Pageable pageable);

    MovieDTO findByImdbID(String imdbID);
}
