package ar.com.edpalmiero.ask4movies.repository.specifications;

import ar.com.edpalmiero.ask4movies.domain.MovieDTO;
import ar.com.edpalmiero.ask4movies.domain.MovieDTO_;
import org.springframework.data.jpa.domain.Specification;

import java.util.Date;

public class MovieSpecifications {

    /*This is done to imitate OMDB api behaviour when filtering by title, but can be modified.*/
    public static Specification<MovieDTO> titleContains(String title) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(MovieDTO_.title), "%" + title + "%");
    }

    public static Specification<MovieDTO> typeLike(String type) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(MovieDTO_.type), type);
    }

    public static Specification<MovieDTO> yearOfRelease(Date yearOfRelease, Date yearOfReleasePlusOne) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.between(root.get(MovieDTO_.released), yearOfRelease, yearOfReleasePlusOne);
    }

}
