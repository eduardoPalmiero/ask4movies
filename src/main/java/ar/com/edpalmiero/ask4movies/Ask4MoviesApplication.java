package ar.com.edpalmiero.ask4movies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ask4MoviesApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ask4MoviesApplication.class, args);
    }

}
