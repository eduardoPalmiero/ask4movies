package ar.com.edpalmiero.ask4movies.serialization;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;

public class CustomDateFormat {
    public static SimpleDateFormat getSimpleDateFormat() {
        final String[] monthsOmdb = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        final String PATTERN = "dd MMM yyyy";
        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols();
        dateFormatSymbols.setMonths(monthsOmdb);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(PATTERN);
        simpleDateFormat.setDateFormatSymbols(dateFormatSymbols);
        return simpleDateFormat;
    }
}
