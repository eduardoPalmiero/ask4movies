package ar.com.edpalmiero.ask4movies.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import static ar.com.edpalmiero.ask4movies.serialization.CustomDateFormat.getSimpleDateFormat;

/*
 * The OMDB API formats the date like '27 May 2019' but in SimpleDateFormat patterns (which Jackson uses, dd MMM yyyy) the month adds a dot (27 may. 2019), and sometimes it gets complicated.
 * */
public class CustomDateDeserializer extends JsonDeserializer<Date> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomDateDeserializer.class);


    @Override
    public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        String dateTrimmed = p.getText().trim();
        try {
            return getSimpleDateFormat().parse(dateTrimmed);
        } catch (ParseException e) {
            LOGGER.error("Failed on parsing Date", e);
        }
        return ctxt.parseDate(dateTrimmed);
    }


}
