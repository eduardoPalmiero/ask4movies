package ar.com.edpalmiero.ask4movies.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Date;

import static ar.com.edpalmiero.ask4movies.serialization.CustomDateFormat.getSimpleDateFormat;

public class CustomDateSerializer extends JsonSerializer<Date> {

    @Override
    public void serialize(Date value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeString(getSimpleDateFormat().format(value));
    }

}
