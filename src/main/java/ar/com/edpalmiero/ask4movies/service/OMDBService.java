package ar.com.edpalmiero.ask4movies.service;

import ar.com.edpalmiero.ask4movies.domain.MovieDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Map;

import static org.apache.logging.log4j.util.Strings.isNotBlank;

@Service
public class OMDBService {
    private static final String API_KEY_PARAM = "apikey";
    private static final String TITLE_PARAM = "t";
    private static final String YEAR_PARAM = "y";
    private static final String IMDB_ID_PARAM = "i";
    @Value("${ask4movies.omdbapi.host}")
    private String uriOmdbApiHost;
    @Value("${ask4movies.omdbapi.apikey}")
    private String apiKeyValue;
    @Value("${ask4movies.omdbapi.scheme}")
    private String omdbApiScheme;

    private RestTemplate restTemplate = new RestTemplate();

    private static final Logger LOGGER = LoggerFactory.getLogger(OMDBService.class);

    public MovieDTO find(Map<String, String> params) {
        return restTemplate.getForObject(prepareRequest(validateParams(params)), MovieDTO.class);
    }

    private URI prepareRequest(MultiValueMap<String, String> params) {
        /*TODO delete log before promoting to production to prevent api key appear in logs*/
        LOGGER.info("Searching in OMDB with params" + params.toString());
        URI uri = UriComponentsBuilder.newInstance()
                .scheme(omdbApiScheme)
                .host(uriOmdbApiHost)
                .queryParams(params)
                .build()
                .toUri();
        LOGGER.info("URI: " + uri);
        return uri;
    }

    private MultiValueMap<String, String> validateParams(Map<String, String> params) {
        MultiValueMap<String, String> paramsOMDB = new LinkedMultiValueMap<>();
        params.entrySet()
                .stream()
                .filter(param -> isNotBlank(param.getValue()))
                .forEach(notNullParam -> {
                    if (notNullParam.getKey().equals("title")) {
                        paramsOMDB.add(TITLE_PARAM, notNullParam.getValue());
                    }
                    if (notNullParam.getKey().equals("yearOfRelease")) {
                        paramsOMDB.add(YEAR_PARAM, notNullParam.getValue());
                    }
                    if (notNullParam.getKey().equals("type")) {
                        paramsOMDB.add("type", notNullParam.getValue());
                    }
                });
        paramsOMDB.add(API_KEY_PARAM, apiKeyValue);
        return paramsOMDB;
    }

    public MovieDTO findById(String imdbID) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add(API_KEY_PARAM, apiKeyValue);
        params.add(IMDB_ID_PARAM, imdbID);
        return restTemplate.getForObject(prepareRequest(params), MovieDTO.class);
    }

}
