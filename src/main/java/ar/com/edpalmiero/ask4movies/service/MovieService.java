package ar.com.edpalmiero.ask4movies.service;

import ar.com.edpalmiero.ask4movies.domain.MovieDTO;
import ar.com.edpalmiero.ask4movies.domain.RatingDTO;
import ar.com.edpalmiero.ask4movies.repository.MovieRepository;
import ar.com.edpalmiero.ask4movies.repository.specifications.MovieSpecifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.apache.logging.log4j.util.Strings.isBlank;
import static org.apache.logging.log4j.util.Strings.isNotBlank;

@Service
public class MovieService {
    @Value("${ask4movies.movieService.resultsPerPage}")
    private int fixedLengthResultsPerPage;

    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private OMDBService omdbService;

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieService.class);

    public MovieDTO findById(String imdbID) {
        if (isBlank(imdbID)) {
            throw new IllegalArgumentException("You must provide a IMDB ID.");
        }
        MovieDTO movieDTO = movieRepository.findByImdbID(imdbID);
        if (movieDTO == null) {
            save(omdbService.findById(imdbID));
        }
        return movieRepository.findByImdbID(imdbID);
    }

    public Page<MovieDTO> findAll(int pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, fixedLengthResultsPerPage);
        return movieRepository.findAll(pageable);
    }

    public Page<MovieDTO> find(Map<String, String> params, int page) {
        Stack<Specification<MovieDTO>> specificationStack = new Stack();
        params.entrySet()
                .stream()
                .filter(param -> isNotBlank(param.getValue()))
                .forEach(param -> specificationStack.push(chooseSpecification(param.getKey(), param.getValue())));

        Pageable pageable = PageRequest.of(page, fixedLengthResultsPerPage);
        Specification specifications = concatSpecifications(specificationStack);
        Page<MovieDTO> movies = movieRepository.findAll(specifications, pageable);
        if (movies.getContent().isEmpty()) {
            getFromOMDBAndSave(params);
            return movieRepository.findAll(specifications, pageable);
        }
        return movies;
    }

    private Specification<MovieDTO> concatSpecifications(Stack<Specification<MovieDTO>> specificationStack) throws EmptyStackException {
        Specification<MovieDTO> specification;
        try {
            specification = specificationStack.pop();
            while (!specificationStack.empty()) {
                specification = specification.and(specificationStack.pop());
            }
        } catch (EmptyStackException e) {
            LOGGER.error("Missing parameters.", e);
            throw e;
        }
        return specification;
    }

    private Specification<MovieDTO> chooseSpecification(String key, String value) {
        if (key.equals("title")) {
            return MovieSpecifications.titleContains(value);
        }
        if (key.equals("yearOfRelease")) {
            Date date = new GregorianCalendar(Integer.parseInt(value), 0, 0).getTime();
            Date datePlusOne = new GregorianCalendar(Integer.parseInt(value) + 1, 0, 0).getTime();
            return MovieSpecifications.yearOfRelease(date, datePlusOne);
        }
        if (key.equals("type")) {
            return MovieSpecifications.typeLike(value);
        }
        throw new IllegalArgumentException("Param not permited" + key);
    }

    @Transactional
    public MovieDTO save(MovieDTO movieDTO) {
        /*This is needed to persist bidirectional relationship*/
        for (RatingDTO ratingDTO : movieDTO.getRatings()) {
            ratingDTO.setMovie(movieDTO);
        }
        LOGGER.debug("Saving in DB from OMDB: " + movieDTO.toString());
        return movieRepository.save(movieDTO);
    }

    private MovieDTO getFromOMDBAndSave(Map<String, String> params) {
        if (isBlank(params.get("title"))) {
            throw new IllegalArgumentException("You should provide a title.");
        }
        LOGGER.info("Going to OMDB to find: " + params.entrySet().toString());
        MovieDTO movie = omdbService.find(params);
        LOGGER.info("Found in OMDB: " + movie.toString());

        return movie.isResponse() ? save(movie) : null;
    }

}
