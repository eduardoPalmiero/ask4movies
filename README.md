# ask4movies
This API is made with Spring Boot and the Spring suite. It is based on OMDB API for first finding movies,series and episodes and keeps them in his own database for avoiding multiple requests to OMDB API.
It only searches in OMDB API if movie is not found so it saves us of making a lot of requests.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
* Java 11
* Maven
* A local database (there's a docker-compose.yml to help you achieve that with a dockerized image of MariaDB).

### Installing

* Clone the repository
```
$ git clone https://gitlab.com/eduardoPalmiero/ask4movies.git
```

* Run DDL scripts in _/scripts_
* Edit application.properties file with your configuration.\
_DON'T FORGET YOUR API_KEY FROM OMDB._
* If you want to change logging files destination, change **fileNamePattern** tag:
```
<rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
    <fileNamePattern>logs/logFile.%d{yyyy-MM-dd}.log</fileNamePattern>
    <maxHistory>30</maxHistory>
    <totalSizeCap>1GB</totalSizeCap>
</rollingPolicy>
```
* In root project folder run Maven goals
```
$ mvn clean install
```
* Run app like JAR, or you can even run it with maven.
```
$ java -jar target\ask4movies-0.0.1-SNAPSHOT.jar
```
```
$ mvn spring-boot:run
```

### Using
The endpoint for finding by imdbID is GET **{{host}}/ask4movies/movie{{imdbID}}**

The endpoint for finding all is GET **{{host}}/ask4movies/movie?p={{pageNumber}}**

_Coming soon: Swagger docs_

#### Query params:
* p -> pageNumber (required)
* t -> title (optional)
* y -> yearOfRelease (optional)
* type -> series,movie,episode (optional)


### Thanks
Hope you enjoy! Feedback and suggestions are welcome.